**Installation**

Clone this repository: 

```
git clone https://gitlab.com/driys/jest-research.git
```

Install dependencies:

jest-cucumber:
```
npm install --save-dev jest jest-cucumber
```

html-reporter:
```
npm install --save-dev jest-html-reporter
```


Setup directory structure:

```
project-root/
│
└── features/
    └── example.feature
│
└── step_definitions/
    └── example.test.js
```


Copy this to package.json

```
  "scripts": {
    "test": "jest --detectOpenHandles"
  },
  "jest": {
    "testEnvironment": "node",
    "testResultsProcessor": "./node_modules/jest-html-reporter"
  }
```

**Getting Started**

To run the Jest tests using Gherkin language, follow these steps:

Write your feature files in the features directory using Gherkin syntax.

Implement step definitions for your feature files in the step_definitions directory.



Run all the Jest tests:

```
npm test
```

Run specific feature:

```
npm test -t "<step_definitions_file_name>"
```

Example:

```
npm test -t "login.test.js"
```

**Writing Feature Files**

Feature files describe the behavior of your application in a human-readable format. They typically follow this structure:

Feature: Description of the feature being tested

  Scenario: Represents a single test case or scenario with concrete inputs and expected outcomes.
    Given [Preconditions or initial state]
    When [Action taken by the user]
    And [Add additional step]
    Then [Expected outcome or result]

  Scenario Outline: Used when you want to run the same scenario multiple times with different inputs.
    Given [Preconditions or initial state]
    When [Action taken by the user] <data1>
    And [Add additional step] <data2>
    Then [Expected outcome or result] <result>

    Examples:
        | data1 | data2 | result      |
        | test  | test  | as expected |
        | abcd  | efgh  | failed      |

You can write multiple scenarios within a feature file to cover different use cases.

**Writing Step Definitions**

Step definitions are JavaScript functions that define the actions performed in each step of the feature file. They are used to map Gherkin steps to executable code. For example:

// Example step definition
```
Given('I want go to X login page', async () => {
    // Implementation code here
});
```

**References**

Gherkin Syntax References: https://cucumber.io/docs/gherkin/

Jest References: https://jestjs.io/docs/getting-started