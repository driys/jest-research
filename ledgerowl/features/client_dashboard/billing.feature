#Billing feature still failed because, login with valid credentials failed to sign in
Feature: Billing

    Background:
        Given User already logged in
        And User has an active company registered to the account

    Scenario: Show and filer data billing

        Given User already on Dashboard page
        When User click menu Billing on sidebar menu
        And User arrow icon on header “Date” to sort ascending
        And User arrow icon on header “Date” to sort descending
        Then User can view the billing list data according to their company
        And User can filter data by date in ascending or descending order

    Background:
        Given User already logged in
        And User has an active company registered to the account
        And User use the billing system
        And User has an unpaid billing invoice

    Scenario Outline: Billing payment using Bank Transfer

        Given User already on Dashboard page
        When User click menu Billing on the sidebar menu
        And User clicks “View” on a transaction with status “Pending”
        And User clicks “Pay Now” button
        And User clicks “Bank Transfer” dropdown list
        And User select <bank> to make a payment
        And User clicks “Pay Now” button
        And User complete the payment process using selected <bank>
        Then User should be able to pay billing using the Bank Transfer method
        And User should redirected to Billing Detail page with Payment Status updated to “Success”
        And Invoice status on Xero should updated to “Paid”

        Examples:
            | bank    |
            | BCA     |
            | Mandiri |
            | Permata |
            | BNI     |
            | BRI     |

    Background:
        Given User already logged in
        And User has an active company registered to the account

    Scenario: Billing payment using QRIS

        Given User already on Dashboard page
        When User click menu Billing on the sidebar menu
        And User clicks “View” on a transaction with status “Pending”
        And User clicks “Pay Now” button
        And User clicks “QRIS” payment method
        And User proceed payment using any QRIS payment method
        Then User should be able to pay billing using QRIS method
        And User should redirected to Billing Detail page with Payment Status “Success”

    Background:
        Given User already logged in
        And User has an active company registered to the account

    Scenario: Wait payment until Snap expired

        Given User already on Dashboard page
        When User click menu Billing on the sidebar menu
        And User clicks “View” on a transaction with status “Pending”
        And User clicks “Pay Now” button
        And User select any payment method
        And User waits until the Snap timer expires
        Then User should redirected to Billing page with the invoice is at the top of the list
        And The transaction status of the corresponding invoice should be updated to "Failed”