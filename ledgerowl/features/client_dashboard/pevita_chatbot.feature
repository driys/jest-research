Feature: Pevita Chatbot

    Background:
        Given User already logged in
        And User has an active company registered to the account
        And The bookkeeping process has started

    Scenario Outline: Upload file using various kind of file

        Given User already on Bookkeeping page
        When User click button Mulai Pembukuan
        And User clicks a document list in “Bank Statement” category on the sidebar
        And User upload submission file <file> for all “Bank Statement” categories
        Then System will check the document submission list on the side bar
        And The document submission progress bar should increase as the document is uploaded
        And System generate the next submission list to upload on the chat windows
        When User clicks a document list in “Revenue Report” category on the sidebar
        And User upload submission file <file> for all “Revenue Report” categories
        Then System will check the document submission list on the side bar
        And The document submission progress bar should increase as the document is uploaded
        And System generate the next submission list to upload on the chat windows
        When User clicks a document list in “Expense Report” category on the sidebar
        And User upload submission file <file> for all “Expense Report” categories
        Then System will check the document submission list on the side bar
        And The document submission progress bar should increase as the document is uploaded
        And System generate the next submission list to upload on the chat windows
        When User clicks a document list in “Others Document” category on the sidebar
        And User upload submission file <file> for all “Others Document” categories
        Then System will check the document submission list on the side bar
        And The document submission progress bar should increase as the document is uploaded
        And User should be able to upload all submission file
        And File should be uploaded to google drive for each category