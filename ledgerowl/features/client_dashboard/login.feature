Feature: Login

  Scenario: Login with valid credential

    Given User goes to the LedgerOwl login page
    When User inputs the phone number "+6283333333333"
    And User clicks the Sign In button
    And User inputs the valid OTP
    And User clicks the Sign In button
    Then The user should be successfully logged in
    And User should be redirected to the LedgerOwl Dashboard page

  Scenario Outline: Login with invalid phone number

    Given User goes to the LedgerOwl login page
    When User inputs the phone number <phone_number> that is not registered
    And User clicks the Sign In button
    Then Login should be failed
    And User should see error message <err_msg>


    Examples:
      | case                 | phone_number | err_msg                  |
      | invalid phone number | 081234567890 | Invalid phone number     |
      | empty phone number   |              | Phone number is required |

  Scenario Outline: Login with invalid OTP

    Given User goes to the LedgerOwl login page
    When User inputs the phone number <phone_number>
    And User clicks the Sign In button
    And User inputs the wrong OTP <otp>
    And User clicks the Sign In button
    Then Login should be failed
    And User should see error message <err_msg>

    Examples:
      | phone_number | otp    | err_msg                                              |
      | 083333333333 | 000000 | Incorrect OTP. Please check your code and try again. |
      | 083333333333 | asdfgh | Incorrect OTP. Please check your code and try again. |
      | 083333333333 | !@#$%^ | Incorrect OTP. Please check your code and try again. |

  Scenario: Login with expired OTP

    Given User goes to the LedgerOwl login page
    When User inputs the phone number "+6283333333333"
    And User clicks the Sign In button
    And User input the expired OTP
    And User clicks the Sign In button
    Then Login should be failed
    And User should see error message "This OTP has expired. Click on resend OTP for a new one."

  Scenario: Resend OTP after 1 minute

    Given User goes to the LedgerOwl login page
    When User inputs the phone number "+6283333333333"
    And User clicks the Sign In button
    And User wait for the Resend counter to reach 02:00
    And User clicks the Resend button
    Then User should be able to click Resend button after 1 minute
    And System should send a new OTP to WhatsApp phone number

  Scenario: Resend OTP after OTP expired

    Given User goes to the LedgerOwl login page
    When User inputs the phone number "+6283333333333"
    And User clicks the Sign In button
    And User waits for the Resend counter to reach 00:00
    And User clicks the Resend button
    Then User should be able to click Resend button after OTP expired
    And System should send a new OTP to WhatsApp phone number

  Scenario: Reload page after session is expired

    Given User already on the Dashboard Page
    When User session token reaches its limit
    And User reload or refresh page
    Then User should be redirected to login page

  # Scenario: Interact with another menu or page after session is expired

  #   Given User already on the Dashboard Page
  #   When User session token reaches its limit
  #   And User click any other menu on side menu
  #   Then User should be redirected to login page

  Scenario: Reload page after session value is deleted

    Given User already on the Dashboard Page
    When User inspect browser element
    And User clicks “Application” tab
    And User clicks “Cookies” menu
    And User clicks on LedgerOwl url
    And User delete “__session” value
    And User reload or refresh page
    Then The user should be able to delete “__session” value
    And User should be redirected to login page