const validLogin = async (page) => {

    await page.goto('https://development.app.ledgerowl.com/auth/login');
    await page.fill('input[placeholder="Enter phone number"]', '83333333333');
    await page.click('button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]');
    await page.fill('input[id="first"]', "1");
    await page.fill('input[id="second"]', "1");
    await page.fill('input[id="third"]', "2");
    await page.fill('input[id="fourth"]', "2");
    await page.fill('input[id="fifth"]', "3");
    await page.fill('input[id="sixth"]', "3");
    await page.click('button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]');
    if(await page.getByText('Dashboard').isVisible()){
        console.log("User successfully logged in");
    }else{
        console.log("Button login not clicked and login failed");
    }
    
    
    
};

module.exports = validLogin;