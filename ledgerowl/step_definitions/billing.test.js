const { defineFeature, loadFeature } = require('jest-cucumber');
const billingFeature = loadFeature('./ledgerowl/features/client_dashboard/billing.feature');
const { chromium } = require('playwright');
const validLogin = require('../support/pages/login');

defineFeature(billingFeature, (test) => {
    let page;
    jest.setTimeout(200000);


    beforeEach(async () => {
        browser = await chromium.launch();
        const context = await browser.newContext();
        page = await context.newPage();
    });

    afterAll(async () => {
        await page.close();
    });

    test('Show and filer data billing', ({ given, and, when, then }) => {
        given('User already logged in', async () => {
            await validLogin(page);
        });

        and('User has an active company registered to the account', async () => {
            // await page.waitForSelector('contains[class="cursor-pointer truncate"]');
            // await page.$('.h-11 > .w-full > .h-full').click();
            // await page.getByText('PT. Kiojio test ganti').click();
        });

        given('User already on Dashboard page', async () => {
            await page.getByText('Dashboard').isVisible();
        });

        when('User click menu Billing on sidebar menu', async () => {
            const element = await page.locator('(//div[contains(@class,"w-full text-start")])[3]').first();
            if (element) {
                await element.click();
                console.log('Menu billing clicked');
            } else {
                console.log('Element not found');
            }

        });

        and('User arrow icon on header “Date” to sort ascending', async () => {
            await page.locator('div[class="flex h-[14px] w-[14px] items-center justify-center stroke-2"]').click();
        });

        and('User arrow icon on header “Date” to sort descending', async () => {
            await page.locator('div[class="flex h-[14px] w-[14px] items-center justify-center stroke-2"]').click();
        });

        then('User can view the billing list data according to their company', async () => {
            console.log('test')
        });

        and('User can filter data by date in ascending or descending order', async () => {
            console.log('test')
        });
    });
});
