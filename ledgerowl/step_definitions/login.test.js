const { defineFeature, loadFeature } = require("jest-cucumber");
const loginFeature = loadFeature(
  "./ledgerowl/features/client_dashboard/login.feature"
);
const { chromium } = require("playwright");
const validLogin = require('../support/pages/login');
const { getByAltText } = require("@testing-library/react");

defineFeature(loginFeature, (test) => {
  let page;
  jest.setTimeout(200000);

  beforeEach(async () => {
    browser = await chromium.launch();
    const context = await browser.newContext();
    page = await context.newPage();
  });

  afterAll(async () => {
    // jest.setTimeout(3000);
    // await page.close();
    console.log("Done");
  });

  test("Login with valid credential", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(/^User inputs the phone number (.*)$/, async () => {
      const inputSelector = 'input[type="tel"]'; // Replace with the actual selector for your input field
      const inputValue = '+62 833 3333 3333';
      await page.type(inputSelector, inputValue);
  
      // Retrieve the value from the field
      const retrievedValue = await page.$eval(inputSelector, el => el.value);
  
      // Assert that the retrieved value matches the input value
      expect(retrievedValue).toBe(inputValue);
      console.log(retrievedValue);
    });

    // button sign-in not clicked
    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    when("User inputs the valid OTP", async () => {
      await page.fill('input[id="first"]', "1");
      await page.fill('input[id="second"]', "1");
      await page.fill('input[id="third"]', "2");
      await page.fill('input[id="fourth"]', "2");
      await page.fill('input[id="fifth"]', "3");
      await page.fill('input[id="sixth"]', "3");
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    then("The user should be successfully logged in", async () => {
      console.log("User successfully logged in");
    });

    then(
      "User should be redirected to the LedgerOwl Dashboard page",
      async () => {
        await page.getByText("Dashboard").isVisible();
      }
    );
  });

  test("Login with invalid phone number", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(
      /^User inputs the phone number (.*) that is not registered$/,
      async (phone_number) => {
        await page.fill(
          'input[placeholder="Enter phone number"]',
          phone_number
        );
      }
    );

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    then("Login should be failed", async () => {
      console.log("Login failed");
    });

    then(/^User should see error message (.*)$/, async (err_msg) => {
      if (err_msg === "Invalid phone number") {
        await expect(page.getByText("Invalid phone number").isVisible());
      } else {
        console.log("" + err_msg);
      }
    });
  });

  test("Login with invalid OTP", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(/^User inputs the phone number (.*)$/, async (phone_number) => {
      await page.fill('input[placeholder="Enter phone number"]', phone_number);
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    when(/^User inputs the wrong OTP (.*)$/, async (otp) => {
      if (otp.length !== 6) {
        console.log("OTP must be a string of 6 characters");
      }
      await page.fill('input[id="first"]', otp[0]);
      await page.fill('input[id="second"]', otp[1]);
      await page.fill('input[id="third"]', otp[2]);
      await page.fill('input[id="fourth"]', otp[3]);
      await page.fill('input[id="fifth"]', otp[4]);
      await page.fill('input[id="sixth"]', otp[5]);
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    then("Login should be failed", async () => {
      console.log("Login failed");
    });

    then(/^User should see error message (.*)$/, async () => {
      await expect(
        page
          .getByText("Incorrect OTP. Please check your code and try again.")
          .isVisible()
      );
    });
  });

  test("Login with expired OTP", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(/^User inputs the phone number (.*)$/, async () => {
      await page.fill('input[placeholder="Enter phone number"]', "83333333333");
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    when("User input the expired OTP", async () => {
      setTimeout(async () => {
        await page.fill('input[id="first"]', "1");
        await page.fill('input[id="second"]', "1");
        await page.fill('input[id="third"]', "2");
        await page.fill('input[id="fourth"]', "2");
        await page.fill('input[id="fifth"]', "3");
        await page.fill('input[id="sixth"]', "3");
      }, 180000);
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    then("Login should be failed", async () => {
      console.log("Login failed");
    });

    then(/^User should see error message (.*)$/, async () => {
      await expect(
        page
          .getByText("This OTP has expired. Click on resend OTP for a new one.")
          .isVisible()
      );
    });
  });

  test("Resend OTP after 1 minute", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(/^User inputs the phone number (.*)$/, async () => {
      await page.fill('input[placeholder="Enter phone number"]', "83333333333");
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    when("User wait for the Resend counter to reach 02:00", async () => {
      const waitForDuration = (durationInSeconds) => {
        return new Promise((resolve) => {
          setTimeout(resolve, durationInSeconds * 1000);
        });
      };

      let countdown = 180; // Start countdown from 3 minutes (180 seconds)

      // Start the countdown
      while (countdown > 120) {
        console.log(`Countdown: ${countdown} seconds`);
        // Wait for 1 second
        await waitForDuration(1);
        countdown--; // Decrease the countdown by 1 second
      }

      // Once the countdown reaches 2 minutes (120 seconds), assert that it reached the expected time
      expect(countdown).toBe(120);
    });

    when("User clicks the Resend button", async () => {
      //Still failed,
      function callback(error, done) {
        if (error) {
          done(error);
          return;
        }
        async function handleClick() {
          try {
            await page.click(
              'div[class="opacity-100 px-2 font-semibold text-blue-primary-500 hover:cursor-pointer"]'
            );
            console.log("clicked");
            done();
          } catch (error) {
            done("Cannot click resend button", error);
          }
        }
        handleClick();
      }
    });

    then(
      "User should be able to click Resend button after 1 minute",
      async () => {
        console.log("User able to click Resend button");
      }
    );

    then("System should send a new OTP to WhatsApp phone number", async () => {
      console.log("New OTP Sent");
    });
  });

  test("Resend OTP after OTP expired", ({ given, when, then }) => {
    given("User goes to the LedgerOwl login page", async () => {
      await page.goto("https://app.ledgerowl.com/auth/login");
    });

    when(/^User inputs the phone number (.*)$/, async () => {
      await page.fill('input[placeholder="Enter phone number"]', "83333333333");
    });

    when("User clicks the Sign In button", async () => {
      await page.click(
        'button[class="montserrat mt-5 flex h-12 w-full items-center justify-center rounded-full bg-blue-primary-500 py-3 text-white lg:w-full"]'
      );
    });

    when("User waits for the Resend counter to reach 00:00", async () => {
      const waitForDuration = (durationInSeconds) => {
        return new Promise((resolve) => {
          setTimeout(resolve, durationInSeconds * 1000);
        });
      };

      let countdown = 180; // Start countdown from 3 minutes (180 seconds)

      // Start the countdown
      while (countdown > 0) {
        console.log(`Countdown: ${countdown} seconds`);
        // Wait for 1 second
        await waitForDuration(1);
        countdown--; // Decrease the countdown by 1 second
      }

      // Once the countdown reaches 0, assert that it reached the expected time
      expect(countdown).toBe(0); // This assertion will be true when countdown reaches 0
    });

    when("User clicks the Resend button", async () => {
      function callback(error, done) {
        if (error) {
          done(error);
          return;
        }
        async function handleClick() {
          try {
            await page.click(
              'div[class="opacity-100 px-2 font-semibold text-blue-primary-500 hover:cursor-pointer"]'
            );
            console.log("clicked");
            done();
          } catch (error) {
            done("Cannot click resend button", error);
          }
        }
        handleClick();
      }
    });

    then(
      "User should be able to click Resend button after OTP expired",
      async () => {
        console.log("User able to click Resend button");
      }
    );

    then("System should send a new OTP to WhatsApp phone number", async () => {
      console.log("New OTP Sent");
    });
  });

  test("Reload page after session is expired", ({ given, when, then }) => {
    given("User already on the Dashboard Page", async () => {
      await validLogin(page);
    });

    when('User session token reaches its limit', async () => {
      await page.evaluate(() => {
        document.cookie = 'session_id=invalid; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      });
    });

    when("User reload or refresh page", async () => {
      await page.reload();
    });

    then(
      "User should be redirected to login page",
      async () => {
        await page.getByText('Sign in with your registered phone number').isVisible();
        // await expect(page.title()).resolves.toMatch('Sign In - Ledgerowl');
      }
    );
  });

  // test("Interact with another menu or page after session is expired", ({ given, when, then }) => {
  //   given("User already on the Dashboard Page", async () => {
  //     await validLogin(page);
  //   });

  //   when('User session token reaches its limit', async () => {
  //     await page.evaluate(() => {
  //       document.cookie = 'session_id=invalid; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
  //     });
  //   });

  //   when("User click any other menu on side menu", async () => {
  //     const targetElement = document.querySelector(".flex w-full");
  //     targetElement.addEventListener('click', function(){

  //     });
  //     targetElement.click();
  //     await page.getByRole('link', { name: 'PT. Kiojio test ganti' }).click();
  //   });

  //   then(
  //     "User should be redirected to login page",
  //     async () => {
  //       await page.getByText('Sign in with your registered phone number').isVisible();
  //       // await expect(page.title()).resolves.toMatch('Sign In - Ledgerowl');
  //     }
  //   );
  // });

  test("Reload page after session value is deleted", ({ given, when, then }) => {
    given("User already on the Dashboard Page", async () => {
      await validLogin(page);
    });

    when('User inspect browser element', async () => {

    });

    when('User clicks “Application” tab', async () => {

    });

    when('User clicks “Cookies” menu', async () => {

    });

    when('User clicks on LedgerOwl url', async () => {

    });


    when('User delete “__session” value', async () => {
      await page.evaluate(() => {
        document.cookie = 'session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      });
    });

    when("User reload or refresh page", async () => {
      await page.reload();
    });

    then('The user should be able to delete “__session” value', async () => {
      const sessionCookie = await page.evaluate(() => {
        return document.cookie.split('; ').find(cookie => cookie.startsWith('session_id='));
      });
      expect(sessionCookie).toBe(null||undefined);
    });

    then(
      "User should be redirected to login page",
      async () => {
        await page.getByText('Sign in with your registered phone number').isVisible();
        // await expect(page.title()).resolves.toMatch('Sign In - Ledgerowl');
      }
    );
  });
});
